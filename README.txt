Field Taxonomy
==============

The field taxonomy module allows you to do two things

1) Select taxonomy terms to attach to fields themselves.

This leads to such fields only being available for a node form if the
node in question has already one of the selected terms assigned.


2) Select taxonomy terms to attach to values for taxonomy fields.


Both functionalities are currently not working with free-tagging
vocabularies.

Usage
=====

a) Create a vocabulary or edit existing ones. Check one or both of the
checkboxes unter "Field taxonomy".

b) Go to admin/content/types and create a node type and assign a field
to it or edit a field on an existing node type.

c) On that widget's form you can see two additional form entries
(assuming you clicked both boxes in a)):

1) "Vocabularies" There you can select from the enabled vocabularies
those that will be available to tag the values of this field in the
node form. This only works for fields of type "multiple".

2) "Terms" There you can select the terms for the widget from
vocabularies enabled for that purpose. The current widget will only be
displayed in the node form if the node has the same term attached
(works on preview). Select "None" if you do not want to use this
feature for this widget.


Developed by Gerhard Killesreiter for Looforyoo SARL.







